# Project name
TAR = fabosamu

SRCDIR   = ./src
TESTDIR  = ./examples

# Compilers
CXX = g++
CXXFLAGS = -std=c++11 -Wall -pedantic -Wno-long-long -O0 -ggdb 
LFLAGS = -Wall -pedantic

# Files
SRCS := $(shell find $(SRCDIR)  -name '*.cpp')
OBJS := $(subst .cpp,.o,$(SRCS))
CMDS := $(TESTDIR)/test_commands.sh

all: clean doc compile

run: $(TAR)
	./$(TAR)

$(TAR): compile

compile: $(OBJS)
	$(CXX) $(LFLAGS) $(OBJS) -o $(TAR) -lcrypto

depend: .depend

.depend: $(SRCS)
	$(RM) ./.depend
	$(CXX) $(CXXFLAGS) -MM $^>>./.depend;

test:
	$(shell  ./examples/test_commands.sh > test.txt)
	# $(shell cat test.txt)

clean:
	-rm -rf doc
	$(RM) *~ .depend
	$(RM) $(OBJS)
	$(RM) $(TAR)


include .depend

.PHONY: doc

doc:
	doxygen Doxyfile
	