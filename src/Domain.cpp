//
// Created by antigravity on 5/29/17.
//


#include "Domain.h"

Domain::Domain()
{}
Domain::Domain(const string & name)
:m_name(name) {}
Domain::~Domain()
{ /*cout << "destroying domain" << endl;*/ }

string Domain::getName() const
{
	return m_name;
}

Domain & Domain::Add( const string & user, const string & pass )
{
	auto entry = make_shared<Entry>(user, pass);

	if ( GetEntry(user) ){
		throw UsernameInDomainExists();
	}
	m_entries.push_back(entry);
	return *this;
}

Domain & Domain::Add( shared_ptr<Entry> entry )
{
	for( auto & x : m_entries ){
		if ( *x == *entry ){
			throw UsernameInDomainExists();
		}
	}
	m_entries.push_back(entry);
	return *this;
}

Domain & Domain::AddNote(const string & user, const string & note)
{
	if ( m_notes.find(user) != m_notes.end() ){
		throw NoteAlreadyInDomain();
	}
	m_notes.insert( {user, note} );
	return *this;
}

Domain & Domain::OverwriteNote(const string & user, const string & note)
{
	try{
		m_notes.at(user) = note;
	} catch (...){
		throw UsernameNotInDomain();
	}
	return *this;
}

void Domain::Erase (const string & user)
{
	for (auto x = m_entries.begin(); x != m_entries.end(); ++x) {

		if (x->get()->getName() == user) {
			m_entries.erase(x);
			try {
				EraseNote(user);
			} catch (...) {}
			return;
		}
	}
	throw UsernameNotInDomain();
}

void Domain::EraseNote(const string & user)
{
	try{
		m_notes.erase(user);
	} catch (...){
		throw UsernameNotInDomain();
	}
}

shared_ptr<Entry> Domain::GetEntry ( const string & user, const string & pass ) const
{
	auto sptr = make_shared<Entry>(user,pass);

	for ( const auto & x : m_entries ){
		if (*x == *sptr){
			return x;
		}
	}
	return nullptr;
}

shared_ptr<Entry> Domain::GetEntry (const string & user ) const
{
	for (auto x = m_entries.begin() ; x != m_entries.end(); ++x) {
		if ( (*x)->getName() == user ){
			return *x;
		}
	}
	return nullptr;
}

void Domain::printRaw() const
{
	for(const auto & x : m_entries){
		cout << "\t" << "(" << x.use_count() << ") " << x.get()->getName() <<" ->"<< x.get()->getPass() << endl;
		try {
			cout << "\t\t" << m_notes.at(x.get()->getName()) << endl;
		} catch (...){}
	}
}

void Domain::printWholeDomain ( ostream & os ) const
{
	os << "\t" << getName() << endl;
	bool isFirst = true;
	for ( auto x : m_entries ){
		if (!isFirst)os << endl;
		isFirst=false;
		os << "\t\t";
		x.get()->printUsername(os);
//			os << " -> ";
//			x.get()->printPassword(os);
		if ( m_notes.find(x.get()->getName()) != m_notes.end() ){
			os<<endl;
			os << "\t\t\t" << m_notes.at(x.get()->getName());
		}
	}
	os<<endl;
}

void Domain::find(const string & toFind, ostream & os) const
{
	if ( getName().find(toFind) != string::npos ){
		printWholeDomain(os);
		return;
	}
	ostringstream ss;
	bool isFirst = true;
	for ( auto x : m_entries ){
		try {
			if ( m_notes.at(x.get()->getName()).find(toFind) != string::npos ){
				if (! isFirst)ss<<endl;
				isFirst=false;
				ss << "\t\t";
				x.get()->printUsername(ss);
				ss << endl;
				string note = m_notes.at(x.get()->getName());
				ss << "\t\t\t" << note;
				continue;
			}
		}catch (...){}
		if ( x.get()->getName().find(toFind) != string::npos ){
//			cout << "TEST: " << m_notes.at(x.get()->getName()) << endl;
			if (! isFirst)ss<<endl;
			isFirst=false;
			ss << "\t\t";
			x.get()->printUsername(ss);
			try {
				ss << endl;
				string note = m_notes.at(x.get()->getName());
				ss << "\t\t\t" << note;
			} catch (...){}
		}
	}
	if ( ss.str() != "" ){
		os << "\t";
		os << getName() << endl;
		os << ss.str();
	}
}

vector<shared_ptr<Record>> Domain::GetSortedRecords()
{
	vector<shared_ptr<Record>> tmp;
	for ( auto x : m_entries ){
		string note;
		try{
			note = m_notes.at(x.get()->getName());
		}catch(...){}
		tmp.push_back( make_shared<Record>( x.get()->getName(), x.get()->getPass(), note ) );
	}
	std::sort(tmp.begin(),tmp.end());
	return tmp;
}