//
// Created by antigravity on 5/27/17.
//

#include "Entry.h"

Entry::Entry() {}

Entry::Entry( const string user, const string pass)
		:m_name(user),
		 m_pass(pass)
{}
Entry::~Entry()
{}

string Entry::getName() const
{
	return m_name;
}
string Entry::getPass() const
{
	return m_pass;
}
void Entry::printUsername( ostream & os )
{
	os << m_name;
}

bool Entry::operator == ( const Entry & right ) const
{
	return m_name == right.getName()
	       && m_pass == right.getPass();
}
bool Entry::operator < (const Entry & src) const
{
	return m_name < src.m_name;
}