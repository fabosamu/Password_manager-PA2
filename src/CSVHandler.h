//
// Created by antigravity on 5/25/17.
//

#ifndef V3_CSVWRITER_H
#define V3_CSVWRITER_H

#include <fstream>
#include "Database.h"
#include "Crypter.h"

/*!
 * handler for CSV files. Inheriting methods from the base class: Database.
 * this is made in expectations of extending the serialization in the future.
 */
class CSVHandler : public Database
{
public:
	/*!
	 * default constructor, getting behaviour of the Database class
	 */
	CSVHandler();
	/*!
	 * taking the filename from the config file, then applying the whole stuff to my own representation of given data.
	 * reading comma separated values from the file, then doing exact operations with them.
	 * @param filename string of the given file
	 */
	void readFile(const string & filename);
	/*!
	 * taking the whole database, inserting it into the *.csv file.
	 * Using edited standard for formatting the inner representation of the data.
	 * @param filename string of the given file
	 */
	void writeFile(const string & filename);

protected:
	/*!
	 * using when writing file, in the formatted way.
	 * @param os stram of file
	 * @param first value
	 * @param second value
	 * @param third value to add
	 */
	void writeLine(ostream & os, const string & first, const string & second = "", const string & third = "");
};


#endif //V3_CSVWRITER_H
