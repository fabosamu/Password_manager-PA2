//
// Created by antigravity on 5/22/17.
//

#ifndef V3_EXCEPTIONS_H
#define V3_EXCEPTIONS_H

#include <exception>

using namespace std;

/*!
 * Base class, inheriting from the main exception class from iostream lib.
 * used only in Database class. The primary message sent can be overwritten inside other code.
 */
class DatabaseEx : public exception
{
public:
	DatabaseEx(const string & message)
			: m_message(message)
	{}

	virtual const char* what() const noexcept
	{
		return m_message.c_str();
	}
private:
	string m_message;
};
class UsernameInDomainExists : public DatabaseEx
{
public:
	UsernameInDomainExists(const string & message = "Username in domain already exists!")
			: DatabaseEx(message) {}
};
class UsernameNotInDomain : public DatabaseEx
{
public:
	UsernameNotInDomain(const string &message = "Cant find username in domain")
			: DatabaseEx(message) {}
};
class ThisDomainExists : public DatabaseEx
{
public:
	ThisDomainExists(const string &message = "This domain already exists!")
			: DatabaseEx(message) {}
};
class DomainDoesntExists : public DatabaseEx
{
public:
	DomainDoesntExists(const string &message = "This domain does not exists!")
			: DatabaseEx(message) {}
};
class NoteAlreadyInDomain : public DatabaseEx
{
public:
	NoteAlreadyInDomain(const string &message = "This username with note is already in domain!")
			: DatabaseEx(message) {}
};



/*!
 * Base class, inheriting from the main exception class from iostream lib.
 * used only when handling any type of file. The primary message sent can be overwritten inside other code.
 */
class FileEx : public exception
{
public:
	FileEx(const string & message)
			: m_message(message)
	{}

	virtual const char* what() const noexcept
	{
		return m_message.c_str();
	}
private:
	string m_message;
};
class BadOutputFile : public FileEx
{
public:
	BadOutputFile(const string &message = "Bad output file!")
			: FileEx(message) {}
};
class BadInputFile : public FileEx
{
public:
	BadInputFile(const string &message = "Bad input file!")
			: FileEx(message) {}
};



/*!
 * exception used only when handling command errors. Such as bad input, typos & stuff.
 */
class BadCommand : public exception
{
public:
	BadCommand(const string & message = "Bad command! for more info use help")
			: m_message(message)
	{}

	virtual const char* what() const noexcept
	{
		return m_message.c_str();
	}
private:
	string m_message;
};

/*!
 * used only when encrypting/decrypting given file.
 */
class BadPasswordSize : public exception
{
public:
	BadPasswordSize(const string & message = "Bad password size! Enter 16-60 characters")
			: m_message(message)
	{}

	virtual const char* what() const noexcept
	{
		return m_message.c_str();
	}
private:
	string m_message;
};


#endif //V3_EXCEPTIONS_H
