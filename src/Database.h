//
// Created by antigravity on 5/19/17.
//

#ifndef V3_DATABASE_H
#define V3_DATABASE_H

#include "Domain.h"

/*!
 * The basis of all work, The Database. Everything is done here, or some layers down.
 * This class is run by the map of domainName as a key and a shared_ptr for domain.
 * mostly calling methods in lower layers.
 * Using references in this method. When an Entry is added, I check all the entries and store only one of the entries,
 * using shared_ptr for knowing what to point at.
 */
class Database
{
public:
	/*!
	 * default constructor
	 */
	Database();
	/*!
	 * default destructor
	 */
	virtual ~Database();
	/*!
	 * method for adding a Domain into the database. Storing objects of Domain in a shared_ptr for easier workaround.
	 * The map is done by the domain name as the key and stored object.
	 * @param Dname key to the map
	 * @return *this for adding more than one
	 */
	virtual Database & Add(const string & Dname);
	/*!
	 * This method adds a user and a password to the specified domain. If there is no domain, method throws.
	 * If this username in given domain exists, this method throws an exception.
	 * Then check all the entries, if there is the same entry, if there is, point at it, if there is not, add new.
	 * @param Dname domain name
	 * @param user unique in domain
	 * @param pass password for given username
	 * @return this
	 */
	virtual Database & Add(const string & Dname, const string & user, const string & pass);
	/*!
	 * Adds a note for given username. More info in class Domain
	 * @param Dname domain name
	 * @param user username
	 * @param note note
	 * @return this
	 */
	Database & AddNote(const string & Dname, const string & user, const string & note);
	/*!
	 * Overwriting EXISTING note. In existing database, for existing username. Else throwing.
	 * @param Dname domain name
	 * @param user username
	 * @param note note to change
	 * @return this
	 */
	Database & OverwriteNote(const string & Dname, const string & user, const string & note);
	/*!
	 * Method for erasing given username in specific domain. Domain must exist, username have to exists.
	 * More info in Domain.
	 * @param Dname domain name
	 * @param user username
	 */
	void Erase(const string & Dname, const string & user);
	/*!
	 * Erases the whole domain, if exists.
	 * @param Dname domain name to erase
	 */
	void Erase(const string & Dname);
	/*!
	 * Erases specific note from database.
	 * @param Dname domain anme
	 * @param user username
	 */
	void EraseNote(const string & Dname, const string & user);
		/*
		 * existuje domena, kde chces menit zaznam?
		 * je v tejto domene uz zaznam s danym pouzivatelom?
		 * zisti refcount pre dany zaznam,
		 *      ak nie, len zmen samotny obsah pointeru
		 *      ak na to ukazuje viacej veci, tak to nejak musim zrusit (erase) a vytvorit novy zaznam s novym heslom
		*/
	/*!
	 * Changes usernames' password.
	 * Checking if domain exists, where I want to change my Entry.
	 * If there is not an Entry with given username, throw.
	 * If there is, erase it and check if there is an Entry to point at.
	 * If so, point, if not, add new.
	 * @param Dname domain name
	 * @param user username
	 * @param pass new password
	 * @return this
	 */
	Database & Change(const string & Dname, const string & user, const string & pass);
	/*!
	 * printing the whole database as it is, with number of references, and passwords.
	 */
	void printAllRaw() const;
	/*!
	 * operator << for printing the whole database, but without references and passwords.
	 * tabbed for more detailed look.
	 * @param os output stream
	 * @param src this
	 * @return output stream
	 */
	friend ostream & operator << ( ostream & os, const Database & src );
	/*!
	 * Finds a specific substring throughout the database. even if it is a substring of:
	 * domain name, username or note.
	 * @param toFind substring to find
	 * @param os output stream
	 */
	void Find( const string toFind, ostream & os) const;
	/*!
	 * Clears the database, used only when debugging. May be implemented.
	 */
	void Clear();

protected:

	map<string, shared_ptr<Domain> > m_domains;
	/*!
	 * gets specific shared_ptr to an Entry, which has the same data as given.
	 * @param user username
	 * @param pass password
	 * @return shared_ptr
	 */
	shared_ptr<Entry> getFromAllEntries (const string & user, const string & pass) const;
	/*!
	 * checks if given domain already is in database.
	 * @param Dname domain name to check
	 */
	void checkAvailability(const string &Dname) const;

};

#endif //V3_DATABASE_H
