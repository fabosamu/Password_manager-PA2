//
// Created by antigravity on 5/29/17.
//

#include "Adder.h"

Adder::Adder(const vector<string> &vec) : Command(vec) {}

string Adder::GetName() const
{
	return "add";
}

void Adder::run()
{
	db.readFile(m_file);

	if ( m_args.size() == 3 && m_args.at(1) == "-d" ){
		transform(m_args.at(2).begin(), m_args.at(2).end(), m_args.at(2).begin(), ::tolower);
		try {
			db.Add(m_args.at(2));
		} catch (...){}
		m_domain = m_args.at(2);
	} else if ( m_args.size() == 4 && m_args.at(1) == "-u" ){
		transform(m_args.at(2).begin(), m_args.at(2).end(), m_args.at(2).begin(), ::tolower);
		db.Add(m_domain, m_args.at(2), m_args.at(3));
	} else if ( m_args.size() == 4 && m_args.at(1) == "-n" ){
		db.AddNote(m_domain, m_args.at(2), m_args.at(3));
	}  else {
		throw BadCommand("use with -d <> ; -u <><> ; -n <><>");
	}
	db.writeFile(m_file);
}