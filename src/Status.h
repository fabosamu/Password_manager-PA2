//
// Created by antigravity on 5/28/17.
//

#ifndef V3_STATUS_H
#define V3_STATUS_H

#include "Command.h"

/*!
 * Command status, user can see, where he "is". Showing database file and domain, last used.
 * everything is done in the run() method
 */
class Status : public Command
{
public:
	/*!
	 * default constructor for this command
	 * @param vec required parameters and phrases for given command
	 */
	Status(const vector<string> &vec);
	/*!
	 * member function which distincts the command by the string (name)
	 * @return string name
	 */
	virtual string GetName() const override;
	/*!
	 * overridden function to run the specific command. this is edited for later use.
	 * showing status message, database and domain I am in.
	 */
	virtual void run() override;
};


#endif //V3_STATUS_H
