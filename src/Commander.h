//
// Created by antigravity on 5/27/17.
//

#ifndef V3_COMMANDER_H
#define V3_COMMANDER_H

#include <iostream>
#include <bits/unique_ptr.h>
#include <unordered_map>
#include "Command.h"
#include "Exceptions.h"

using namespace std;

/*!
 * this is the main class, which stores all inserted commands in a map of unique pointers.
 * On every command should I point only once. Thats why.
 * This class is also extendable for other operations, if there were more commands,
 * than the programmer could stand.
 */
class Commander
{

public:
	Commander();
	~Commander();

	/*!
	 * This is method used in main.cpp, for adding every single new command.
	 * It moves the Command object to the prepared map of Commands, where the command is stored and used later.
	 * @param cmd the Command object.
	 */
	void makeCommand(unique_ptr<Command> && cmd);
	/*!
	 * Method for doing the exact command, which is mapped by a string.
	 * First I grab filename, where is my whole database from the hidden file .config,
	 * then I do the command
	 * after what I take all .config values I need to have (filename of domain, set domain and password if necessary)
	 * @param cmd the whole unique_ptr.
	 */
	void doCommand(const string & com);
protected:

	unordered_map<string, unique_ptr<Command>> m_commands;
	/*!
	 * Private method for checking, whether the commands is in the map, or not.
	 * Used in public method: Commander::doCommand.
	 * @param com string, as a key to the map.
	 */
	void checkCommand(const string &com);
};


#endif //V3_COMMANDER_H
