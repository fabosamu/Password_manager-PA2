//
// Created by antigravity on 5/27/17.
//

#ifndef V3_ADDER_H
#define V3_ADDER_H


#include "Command.h"
#include "Exceptions.h"
/*!
 * Command add, user can add username and password, note for username and add/set domain
 * everything is done in the run() method
 */
class Adder : public Command
{
public:
	/*!
	 * default constructor for this command
	 * @param vec required parameters and phrases for given command
	 */
	Adder(const vector<string> &vec);
	/*!
	 * member function which distincts the command by the string (name)
	 * @return string name
	 */
	virtual string GetName() const override;
	/*!
	 * overridden function to run the specific command. this is edited for later use.
	 * when adding a domain, this also sets the default domain, which is later stored int he configuration file.
	 * using methods in the created database class
	 */
	virtual void run() override;
};


#endif //V3_ADDER_H
