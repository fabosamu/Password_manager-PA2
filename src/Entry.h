//
// Created by antigravity on 5/18/17.
//

#ifndef V3_ENTRY_H
#define V3_ENTRY_H

#include <iostream>

using namespace std;

/*!
 * This is a class, where the exact pieces of data are represented.
 * This object can be compared, also has a operator < form sorting...
 */
class Entry
{
public:
	/*!
	 * default constructor
	 */
	Entry();
	/*!
	 * cunstructor, where setting:
	 * @param user username
	 * @param pass password
	 */
	Entry( const string user, const string pass);
	/*!
	 * virtual destructor
	 */
	virtual ~Entry();
	/*!
	 * returns a single string as a name
	 * @return string - username
	 */
	virtual string getName() const;

	/*!
	 * returns a single password
	 * @return password
	 */
	virtual string getPass() const;
	/*!
	 * prints username only to the output stream
	 * @param os output stream
	 */
	void printUsername( ostream & os );
	/*!
	 * operator == for comparing two Entries, mainly the usernames.
	 * @param right Entry
	 * @return true / false
	 */
	bool operator == ( const Entry & right ) const;
	/*!
	 * default operator < using when sorting entries and records
	 * @param src Entry
	 * @return true when is less
	 */
	virtual bool operator < (const Entry & src) const;

protected:
	string m_name;
	string m_pass;
};


#endif //V3_ENTRY_H
