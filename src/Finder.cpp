//
// Created by antigravity on 5/29/17.
//

#include "Finder.h"

Finder::Finder(const vector<string> &vec)
		: Command(vec)
{}
string Finder::GetName() const
{
	return "find";
}

void Finder::run()
{
	db.readFile(m_file);
	if ( m_args.size() == 2 ) {
		stringstream ss;
		db.Find(m_args.at(1),ss);
		cout << ss.str() << endl;
	} else {
		throw BadCommand();
	}
}