//
// Created by antigravity on 5/27/17.
//

#ifndef V3_INITIALIZER_H
#define V3_INITIALIZER_H

#include "Command.h"
/*!
 * Command init, user can initialize the database file, or change working database file.
 * everything is done in the run() method
 */
class Initializer : public Command
{
public:
	/*!
	 * default constructor for this command
	 * @param vec required parameters and phrases for given command
	 */
	Initializer(const vector<string> &vec);
	/*!
	 * member function which distincts the command by the string (name)
	 * @return string name
	 */
	virtual string GetName() const override;
	/*!
	 * overridden function to run the specific command. this is edited for later use.
	 * removes config file, if necessary (when changing database file)
	 * or creates given file, when it does not exists
	 */
	virtual void run() override;
};


#endif //V3_INITIALIZER_H
