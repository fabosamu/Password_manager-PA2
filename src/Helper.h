//
// Created by antigravity on 5/27/17.
//

#ifndef V3_HELPER_H
#define V3_HELPER_H


#include "Command.h"
/*!
 * Command help, user gets the proper help for using this program.
 * everything is done in the run() method
 */
class Helper : public Command
{
public:
	/*!
	 * using when printing only small-help
	 */
	Helper();
	/*!
	 * other constructor for this command
	 * @param vec required parameters and phrases for given command
	 */
	Helper(const vector<string> & vec);
	/*!
	 * member function which distincts the command by the string (name)
	 * @return string name
	 */
	string GetName() const;
	/*!
	 * when empty arguments are given, shows only small-help.
	 * if there is the name of the command in the arguments, this shows the proper help-page for this program.
	 */
	virtual void run() override;
};


#endif //V3_HELPER_H
