//
// Created by antigravity on 5/29/17.
//

#include "Helper.h"

Helper::Helper() {}

Helper::Helper(const vector<string> & vec)
		:Command(vec)
{}

string Helper::GetName() const
{
	return "help";
}

void Helper::run()
{
	if ( m_args.size() != 1 ){
		cout << "Invalid input! Use help for more info" << endl;
	} else {
		cout << "This is a help-page for program v3:\n"
				"******************************************************************************\n"
				"for more information see documentation pages.\n"

				"COMMANDS: help init add erase status set find show\n"

				"help\t: shows this help-page.\n"

				"init\t: <FILENAME> sets the base of the database.\n"

				"add\t: adds user, domain or note to the storage. Use with:\n"
				"\t -d <domain name> as domain, also setting main domain for later adding\n"
				"\t -u <username> <password> as user\n"
				"\t -n <username> <note> as note\n"

				"erase\t: erases user, domain or note from the storage. Use with:\n"
				"\t -d <domain name> as domain\n"
				"\t -u <username> <password> as user\n"
				"\t -n <username> <note> as note\n"

				"status\t: shows name of main storage and main domain\n"

				"set\t: sets or changes main domain or password\n"
				"\t -d <domain name> sets main domain for adding\n"
				"\t -u <username><password> changes password of specific username\n"
				"\t -n <username><note>\n"

				"find\t: <EXPRESSION> finds specific expression through the database\n"

				"show\t: shows actual database of stored passwords.\n"
				"\t -p shows actual database with passwords\n"

				"******************************************************************************"
		     << endl;
	}
}
