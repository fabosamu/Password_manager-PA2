//
// Created by antigravity on 6/4/17.
//

#include "Crypter.h"


Crypter::Crypter() {}

string Crypter::Encrypt(const string & plain, const string &password)
{
	if ( password.size() < 16 || password.size() >= EVP_MAX_KEY_LENGTH ){
		throw BadPasswordSize();
	}

	unsigned char *  ot = new unsigned char[ (plain.length() + 1) *2 ];  // original string
    strcpy( (char *)ot, plain.c_str() );
    unsigned char st[65536];  // encrypted text
    unsigned char key[EVP_MAX_KEY_LENGTH];  // key for encryption

    for ( int i = 0 ; i < (int)password.size() ; ++i ){
    	key[i] = password[i];
    }

    // unsigned char hash[EVP_MAX_IV_LENGTH];
    int length = 0;
    unsigned char iv[EVP_MAX_IV_LENGTH];  // inicialise vector
    doHash( password.c_str(), iv, length );

    const char cipherName[] = "RC4";
    const EVP_CIPHER * cipher;

    OpenSSL_add_all_ciphers();
    cipher = EVP_get_cipherbyname(cipherName);

    int otLength = strlen((const char*) ot);
    int stLength = 0;
    int tmpLength = 0;

    EVP_CIPHER_CTX ctx;

    // encrypting
    EVP_EncryptInit( &ctx, cipher, key, iv );
    EVP_EncryptUpdate( &ctx,  st, &tmpLength, ot, otLength );
    stLength += tmpLength;
    EVP_EncryptFinal( &ctx, st + stLength, &tmpLength );
    stLength += tmpLength;

    string str( st, st + stLength );

    //cleaning memory
    EVP_cleanup();
    EVP_CIPHER_CTX_cleanup(&ctx);
    delete[] ot;
    return str;
}

string Crypter::Decrypt(const string & cipher, const string &password)
{
	if (password.size() < 16 || password.size() >= EVP_MAX_KEY_LENGTH ){
		throw BadPasswordSize();
	}

    unsigned char * st = new unsigned char[ cipher.length() + 1 ];  // encrypted string
    strcpy( (char *)st, cipher.c_str() );
    unsigned char dt[65536];  // original string
    unsigned char key[EVP_MAX_KEY_LENGTH];   // key for decryption

    for ( int i = 0 ; i < (int)password.size() ; ++i ){
    	key[i] = password[i];
    }
    // unsigned char hash[EVP_MAX_IV_LENGTH];
    int length = 0;
    unsigned char iv[EVP_MAX_IV_LENGTH];  // inicialise vector
    doHash( password.c_str(), iv, length );

    const char cipherName[] = "RC4";
    const EVP_CIPHER * ciphertext;

    OpenSSL_add_all_ciphers();
    ciphertext = EVP_get_cipherbyname(cipherName);

    int stLength = strlen( (const char*) st );
    int dtLength = 0;
    int tmpLength = 0;

    EVP_CIPHER_CTX ctx;

    // decrypting
    EVP_DecryptInit(&ctx, ciphertext, key, iv);
    EVP_DecryptUpdate(&ctx, dt, &tmpLength,  st, stLength);
    dtLength += tmpLength;
    EVP_DecryptFinal(&ctx, dt + dtLength, &tmpLength);
    dtLength += tmpLength;

    string str(dt, dt + dtLength);

    //cleaning memory
    EVP_cleanup();
    EVP_CIPHER_CTX_cleanup(&ctx);
    delete[] st;
    return str;
}


void Crypter::doHash ( const char * text, unsigned char * hash, int & length)
{
	// todo: sha256
	char hashFunction[] = "md5";  // choose a hash function ("sha1", "md5" ...)

	EVP_MD_CTX ctx;  // structure of context
	const EVP_MD *type; // type for using the hash function

	/* Inicialize OpenSSL hash function */
	OpenSSL_add_all_digests();
	/* Check, what hash func has to be used */
	type = EVP_get_digestbyname(hashFunction);

	/* if previous func has returned -1, handle it */
	if(!type) {
		printf("Hash %s doesnt exists.\n", hashFunction);
		exit(1);
	}

	/* hash */
	EVP_DigestInit(&ctx, type);  // nastaveni kontextu
	EVP_DigestUpdate(&ctx, text, strlen(text));  // zahashuje text a ulozi do kontextu
	EVP_DigestFinal(&ctx, hash, (unsigned int *) &length);  // ziskani hashe z kontextu


//	 EVP_CIPHER_CTX_cleanup();
	EVP_cleanup();

}
