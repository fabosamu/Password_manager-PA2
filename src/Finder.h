//
// Created by antigravity on 5/28/17.
//

#ifndef V3_FINDER_H
#define V3_FINDER_H


#include "Command.h"
/*!
 * Command find, user can find substring of anything in the database. Even if it is a part of domain name,
 * username or note. I dont check passwords.
 * everything is done in the run() method
 */
class Finder : public Command
{
public:
	/*!
	 * default constructor for this command
	 * @param vec required parameters and phrases for given command
	 */
	Finder(const vector<string> &vec);
	/*!
	 * member function which distincts the command by the string (name)
	 * @return string name
	 */
	virtual string GetName() const override;
	/*!
	 * overridden function to run the specific command. this is edited for later use.
	 * plus-minus checking only syntax of the command. everything is done in lower layer.
	 */
	virtual void run() override;
};


#endif //V3_FINDER_H
