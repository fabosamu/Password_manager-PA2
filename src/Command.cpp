//
// Created by antigravity on 5/29/17.
//

#include "Command.h"

Command::Command()
{}

Command::Command(const vector<string> & vec)
		:m_args(vec)
{}

Command::~Command()
{}

void Command::getConfig()
{
	ifstream in (".config", ios::in);
	if (!in){
		in.close();
		ofstream out (".config", ios::out | ios::app);
		out.close();
	}
	getline(in, m_file);
	getline(in, m_domain);
	getline(in, m_pass);
	in.close();
}

void Command::setConfig()
{
	ofstream out (".config", ios::out);
	if (!out){
		throw BadOutputFile("bad config file: set");
	}
	out << m_file << endl;
	out << m_domain << endl;
	out << m_pass << endl;
	out.close();
}
