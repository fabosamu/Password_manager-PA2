//
// Created by antigravity on 5/27/17.
//

#ifndef V3_COMMAND_H
#define V3_COMMAND_H

#include <iostream>
#include <vector>
#include <fstream>
#include "Exceptions.h"
#include "CSVHandler.h"

/*!
 * BASE CLASS
 * as a command in general. Almost every method is virtual, used by lower layers.
 * This is used as a standard, also as a product of extendation.
 */
class Command
{
public:
	/*!
	 * default constructor, nothing special, used (only) when providing command "help"
	 */
	Command();
	/*!
	 * Constructor used when creating child commands, this is, where I add all the parameters
	 * @param vec vector of strings -> parameters of the command
	 */
	Command(const vector<string> & vec);
	/*!
	 * standard virtual destructor
	 */
	virtual ~Command();
	/*!
	 * defined in child classes,
	 * @return single string = the name of command itself
	 */
	virtual string GetName() const = 0;
	/*!
	 * the most essential method.
	 * this is the place, where I parse all the parameters from the command line
	 * and running needed commands on the database.
	 */
	virtual void run() = 0;
	/*!
	 * getting data from configuration file to the class variables
	 * if the config file doesnt exist, this method creates one empty.
	 * if it doesnt contain enough data, the corresponding private variables (strings) would be left empty.
	 */
	virtual void getConfig();
	/*!
	 * setting the configuration file. Leaves three newlines, if the variables got from the previous method
	 * were empty.
	 */
	virtual void setConfig();

protected:
	CSVHandler db;
	vector<string> m_args;
	string m_domain, m_file, m_pass;
};


#endif //V3_COMMAND_H
