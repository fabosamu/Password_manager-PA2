//
// Created by antigravity on 5/27/17.
//

#ifndef V3_SHOWER_H
#define V3_SHOWER_H

#include <iostream>
#include <unistd.h>
#include "Command.h"
#include "CSVHandler.h"


/*!
 * Command add, user can add username and password, note for username and add/set domain
 * everything is done in the run() method
 */
class Shower : public Command
{
public:
	/*!
	 * default constructor for this command
	 * @param vec required parameters and phrases for given command
	 */
	Shower(const vector<string> & vec);
	/*!
	 * member function which distincts the command by the string (name)
	 * @return string name
	 */
	virtual string GetName() const override;
	/*!
	 * overridden function to run the specific command. this is edited for later use.
	 * basically showing what all the database contains. Can show also passwords.
	 */
	virtual void run() override;
};


#endif //V3_SHOWER_H
