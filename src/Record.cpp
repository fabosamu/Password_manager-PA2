//
// Created by antigravity on 5/29/17.
//

#include "Record.h"

Record::Record(const string & user, const string & pass, const string & note)
		:Entry(user, pass), m_note(note)
{}

string Record::getNote() const
{
	return m_note;
}