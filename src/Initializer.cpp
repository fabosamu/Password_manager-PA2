//
// Created by antigravity on 5/29/17.
//

#include "Initializer.h"

Initializer::Initializer(const vector<string> &vec) : Command(vec) {}

string Initializer::GetName() const
{
	return "init";
}

void Initializer::run()
{
//		todo: odstranenie configu
	ofstream rm (".config", ios::out);
	rm << "";
	rm.close();
	if ( m_args.size() != 2 ){
		throw BadCommand();
	}
	m_file = m_args.at(1);
	m_domain = "";
	ofstream file ( m_file, ios::out | ios::app);
	file.close();
}