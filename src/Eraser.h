//
// Created by antigravity on 5/28/17.
//

#ifndef V3_ERASER_H
#define V3_ERASER_H


#include "Command.h"
/*!
 * Command for erasing, user can erase username and password, note for username and add/set domain
 * everything is done in the run() method
 */
class Eraser : public Command
{
public:
	/*!
	 * default constructor for this command
	 * @param vec required parameters and phrases for given command
	 */
	Eraser(const vector<string> &vec);
	/*!
	 * member function which distincts the command by the string (name)
	 * @return string name
	 */
	virtual string GetName() const override;
	/*!
	 * overridden function to run the specific command.
	 * user can erase the whole domain, is prompted before doing so,
	 * with other parameters user can erase specific username with its password
	 * or username's note.
	 * everything is done in the lower layers
	 */
	virtual void run() override;
};


#endif //V3_ERASER_H
