//
// Created by antigravity on 6/4/17.
//

#ifndef V3_CRYPTER_H
#define V3_CRYPTER_H

#include <iostream>
#include <string>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>
#include "Exceptions.h"
#include <fstream>


using namespace std;

/*!
 * This class provides the whole crypting and hashing used in this program.
 * Using this class only when reading and writing to the file, but can be used to almost everything.
*/
class Crypter
{
public:
	/*!
	 * Default constructor
	 */
	Crypter();
	 /*!
	  * This method gets its text, which should be encrypted, then puts out the crypted string.
	  * Using functions from library openSSL
	  * @param plain string, to encrypt
	  * @param key to encrypt
	  * @return ciphertext
	  */
	string Encrypt( const string & plain, const string & password );
	 /*!
	  * This method decrypts cipher text by given passcode. Using hash sha1 for initialize vector.
	  * Using functions from library openSSL
	  * @param ciphertext to decrypt
	  * @param key to decrypt
	  * @return plaintext
	  */
	string Decrypt( const string & cipher, const string & password );

protected:
	string m_filename, m_password;
	 /*!
	  * This method hashes given phrase. Output hash is 16bytes long.
	  * Using functions from library openSSL
	  * @param plain string, to hash
	  * @param hashed plaintext
	  * @param length of hash (in case of changing the hash funct)
	  * @return hash string
	  */
	void doHash ( const char * text, unsigned char * hash, int & length);
};


#endif //V3_CRYPTER_H
