//
// Created by antigravity on 5/29/17.
//

#include "Database.h"

Database::Database()
{}
Database::~Database()
{}

/*!
 * Adding domain to map, only checking if there already is a domain with given name.
 * refcount doesnt have to apply, thats why I store unique_ptrs
 * @param dsptr is shared_ptr to domain on heap created in upper layer
 * @return for adding more than one domain at once
 */
Database & Database::Add(const string & Dname)
{
//		cout << "adding Domain" << endl;
	auto domain = make_shared<Domain>(Dname);
	if ( m_domains.count(Dname) > 0 ){
		throw ThisDomainExists();
	}
	m_domains.insert( {Dname, domain} );
	return *this;
}

Database & Database::Add(const string & Dname, const string & user, const string & pass)
{
	checkAvailability(Dname);
	auto &domain = m_domains.at(Dname);
	if (domain.get()->GetEntry(user)) {
		throw UsernameInDomainExists();
	}

	auto dbEntry = getFromAllEntries(user, pass);

	if (dbEntry) {
		domain->Add(dbEntry);
	} else {
		domain->Add(user, pass);
	}
	return *this;
}

Database & Database::AddNote(const string & Dname, const string & user, const string & note)
{
	checkAvailability(Dname);
	auto domain = m_domains.at(Dname).get();
	if ( ! domain->GetEntry(user)) {
		throw UsernameNotInDomain();
	}
	domain->AddNote(user, note);
	return *this;
}

Database & Database::OverwriteNote(const string & Dname, const string & user, const string & note)
{
	checkAvailability(Dname);
	auto domain = m_domains.at(Dname).get();
	if ( ! domain->GetEntry(user)) {
		throw UsernameNotInDomain();
	}
	domain->OverwriteNote(user, note);
	return *this;
}

void Database::Erase(const string & Dname, const string & user)
{
	checkAvailability(Dname);
	m_domains.at(Dname).get()->Erase(user);
}
void Database::Erase(const string & Dname)
{
	checkAvailability(Dname);
	m_domains.erase(Dname);
}

void Database::EraseNote(const string & Dname, const string & user)
{
	checkAvailability(Dname);
	m_domains.at(Dname).get()->EraseNote(user);
}
/*
 * existuje domena, kde chces menit zaznam?
 * je v tejto domene uz zaznam s danym pouzivatelom?
 * zisti refcount pre dany zaznam,
 *      ak nie, len zmen samotny obsah pointeru
 *      ak na to ukazuje viacej veci, tak to nejak musim zrusit (erase) a vytvorit novy zaznam s novym heslom
*/
Database & Database::Change(const string & Dname, const string & user, const string & pass)
{
	checkAvailability(Dname);
	auto & domain = m_domains.at(Dname);

	if ( domain.get()->GetEntry(user, pass) ){
		throw UsernameNotInDomain();
	}

	domain->Erase(user);
	auto dbE = getFromAllEntries(user, pass);
	if (dbE){
		domain->Add(dbE);
	} else {
		domain->Add(user, pass);
	}
	return *this;
}
void Database::printAllRaw() const
{
	for (const auto & x : m_domains){
		cout << x.first << endl;
		x.second.get() -> printRaw();
	}
}

ostream & operator << ( ostream & os, const Database & src )
{
	for ( auto & x : src.m_domains ) {
		x.second.get()->printWholeDomain(os);
	}
	return os;
}



void Database::Find( const string toFind, ostream & os) const
{
	os << "Search results:\n";
	for ( auto x : m_domains ){
		stringstream ss;
		x.second.get()->find(toFind,ss);
		if ( ss.str() != "" ){
			os << ss.str()<<endl;
		}
	}
	return;
}

void Database::Clear()
{
	m_domains.clear();
}

shared_ptr<Entry> Database::getFromAllEntries (const string & user, const string & pass) const
{
	for ( auto & x : m_domains ){
		auto tmp = x.second.get()->GetEntry(user, pass);
		if (tmp) {
			return tmp;
		}
	}
	return nullptr;
}

void Database::checkAvailability(const string &Dname) const
{
	if ( m_domains.count(Dname) == 0 ){
		throw DomainDoesntExists();
	}
}
