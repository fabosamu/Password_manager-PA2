//
// Created by antigravity on 5/29/17.
//
#include "CSVHandler.h"

#define PASS "01234567890123456789"

CSVHandler::CSVHandler()
	:Database() {}

void CSVHandler::writeFile(const string & filename)
{
	stringstream os;
	for ( auto x : m_domains ){
		writeLine(os, x.first);
		auto tmp = x.second.get()->GetSortedRecords();
		for (auto y : tmp){
			writeLine( os, y.get()->getName(), y.get()->getPass(), y.get()->getNote() );
		}
	}

	ofstream out ( filename, ios::out );
	if (!out) throw BadOutputFile();

	Crypter ecr;
	out << ecr.Encrypt(os.str(), PASS);
	out.close();
}

void CSVHandler::readFile(const string & filename)
{

	ifstream input (filename, ios::in);
	if (!input) throw BadInputFile();

	string content( (istreambuf_iterator<char>(input) ),
                    (istreambuf_iterator<char>()    ) );
	input.close();

	Crypter dcr;
	stringstream in;
	in << dcr.Decrypt(content, PASS);

	if ( in ){
		string line;
		string domain;
		while ( getline( (in), line) ){
			stringstream ss;
			ss.str(line);
			vector<string> vec;
			for( string str ; getline( ss, str, ',' ) ; ){
				vec.push_back(str);
			}
			switch ( vec.size() ){
				case 1:
					Add(vec[0]);
					domain = vec[0];
					break;
				case 2:
					Add(domain, vec[0], vec[1]);
					break;
				case 3:
					Add(domain, vec[0], vec[1]);
					AddNote(domain, vec[0], vec[2]);
					break;
				default:
					throw BadInputFile("Something bad happened to the database file.");
			}
		}
	} else {
		throw BadInputFile("Bad input file(s)! Try init");
	}
}


void CSVHandler::writeLine(ostream & os, const string & first, const string & second, const string & third)
{
	if ( second.empty() && third.empty() ){
		os << first << endl;
	} else {
		os << first << "," << second << "," << third << "\n";
	}
}