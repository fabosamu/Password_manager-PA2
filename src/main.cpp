//
// Created by antigravity on 5/27/17.
//

#include <iostream>
#include <vector>
#include <memory>
#include "Commander.h"
#include "Helper.h"
#include "Adder.h"
#include "Initializer.h"
#include "Status.h"
#include "Eraser.h"
#include "Finder.h"
#include "Setter.h"
#include "Shower.h"
/*!
 * in this function I am checking, if user is inserting any bad characters,
 * which could potentially harm my database.
 * Using standard string functions and a lambda function.
 * This is a edited standard for domain names, usernames, passwords etc.
 *
 * @param str checking one string only. have to go through whole bunch of input
 * @return bool value, true for valid string, false for invalid.
 */
bool string_is_valid(const std::string &str)
{
//	 _, -. +, =,!, @, %, *, &, ”, :, ., or /
	return find_if(str.begin(), str.end(),
	       [](char c) {
		         return !(isalnum(c) || (c == '_') || (c == '-') || (c == '+') || (c == '=') || (c == '!')
		                             || (c == '@') || (c == '%') || (c == '*') || (c == '&') || (c == '"')
		                             || (c == ':') || (c == '.') || (c == '/') || (c == ' '));
	               }) == str.end();
}

/*!
 * The main program, I use bash/shell console for inputting any commands, thats why I dont have to check
 * completely everything possible inputs, only those, which could be harmbful to my database.
 * Standard C-style input
 *
 * using Commander as main class to handle every single command, which is in.
 * This is also a standard for this type of program input handling, but simplified.
 * @param argc number of arguments
 * @param argv strings of arguments
 * @return standard value 0 for accomplishment, 1 for error. Others may (should not) occur,
 * because of crypting tools.
 */
int main (int argc, const char ** argv)
{
	vector<string> vec (argv+1, argv+argc);
	for ( auto && x : vec ){
		if ( ! string_is_valid(x) ){
			cout << "bad formatting!" << endl;
			return 1;
		}
	}
	Commander cmdr;

	if ( argc > 1 ){
		cmdr.makeCommand(unique_ptr<Command>(new Helper(vec)));
		cmdr.makeCommand(unique_ptr<Command>(new Initializer(vec)));
		cmdr.makeCommand(unique_ptr<Command>(new Adder(vec)));
		cmdr.makeCommand(unique_ptr<Command>(new Status(vec)));
		cmdr.makeCommand(unique_ptr<Command>(new Shower(vec)));
		cmdr.makeCommand(unique_ptr<Command>(new Eraser(vec)));
		cmdr.makeCommand(unique_ptr<Command>(new Finder(vec)));
		cmdr.makeCommand(unique_ptr<Command>(new Setter(vec)));

		try {
			cmdr.doCommand(vec[0]);
		} catch ( exception & e ){
//			printing error messages catched from outlayers.
			cout << e.what() << endl;
			return 1;
		} catch (...) {
//			printing standard error message
			Helper().run();
			return 1;
		}
	} else {
		Helper().run();
		// return 1;
	}
	return 0;
}