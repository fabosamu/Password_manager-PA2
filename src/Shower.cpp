//
// Created by antigravity on 5/29/17.
//

#include "Shower.h"

Shower::Shower(const vector<string> & vec)
	:Command(vec)
{}

string Shower::GetName() const
{
	return "show";
}

void Shower::run()
{
	db.readFile(m_file);

	if ( m_args.size() == 1 ){
		cout << "\tDOMAIN\tUSER\tNOTE" << endl;
		cout << m_file << ":" << endl;
		cout << db << endl;
	} else if ( m_args.size() == 2 && m_args.at(1) == "-p" ){
		cout << "DOMAIN\tUSER\tNOTE" << endl;
//		todo: dorob checkovanie passwordu, zmena bude na prikaz set
		db.printAllRaw();
	}  else {
		throw BadCommand();
	}
}