//
// Created by antigravity on 5/29/17.
//

#include "Eraser.h"

Eraser::Eraser(const vector<string> &vec) : Command(vec) {}

string Eraser::GetName() const
{
	return "erase";
}

void Eraser::run()
{
	db.readFile(m_file);
	try {
		transform(m_args.at(2).begin(), m_args.at(2).end(), m_args.at(2).begin(), ::tolower);
	}catch(...){}
	if ( m_args.size() == 3 && m_args.at(1) == "-d" ){
		cout << "Do you really want to erase the whole domain?(y/n)" << endl;
		string prpt;
		cin >> prpt;
		if (prpt.size() && (prpt[0] == 'y' || prpt[0] == 'Y')){
			db.Erase(m_args.at(2));
		}
	} else if ( m_args.size() == 3 && m_args.at(1) == "-u" ){
		db.Erase(m_domain, m_args.at(2));
	} else if ( m_args.size() == 3 && m_args.at(1) == "-n" ){
		db.EraseNote(m_domain, m_args.at(2));
	}  else {
		throw BadCommand();
	}
	db.writeFile(m_file);
}