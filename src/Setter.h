//
// Created by antigravity on 5/29/17.
//

#ifndef V3_SETTER_H
#define V3_SETTER_H


#include "Command.h"
/*!
 * Command set, user can set main domain for adding/erasing, changing specific notes for users, also changing passwords.
 * Including encrypting and decrypting the file with the database in
 * everything is done in the run() method
 */
class Setter : public Command
{
public:
	/*!
	 * default constructor for this command
	 * @param vec required parameters and phrases for given command
	 */
	Setter(const vector<string> &vec);
	/*!
	 * member function which distincts the command by the string (name)
	 * @return string name
	 */
	virtual string GetName() const override;
	/*!
	 * overridden function to run the specific command. this is edited for later use.
	 * can set domain, change password or overwrite note for specific username.
	 * Encryption and decryption included
	 */
	virtual void run() override;
};


#endif //V3_SETTER_H
