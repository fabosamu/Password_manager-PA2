//
// Created by antigravity on 5/18/17.
//

#ifndef V3_DOMAIN_H
#define V3_DOMAIN_H

#include <iostream>
#include <vector>
#include <memory>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
#include <sstream>
#include <list>
#include <algorithm>
#include "Entry.h"
#include "Exceptions.h"
#include "Record.h"

using namespace std;

/*!
 * This class stores all the entries in a vector of shared_ptrs, done because of saving space when providing references.
 * Basically only an object to store data. There may be more domains.
 */
class Domain
{
public:
	/*!
	 * default constructor
	 */
	Domain();
	/*!
	 * constructor also with adding a name of a domain
	 * @param name name of the domain
	 */
	Domain(const string & name);
	/*!
	 * default destructor
	 */
	~Domain();
	/*!
	 * Simple getter for domain name
	 * @return name of the domain
	 */
	string getName() const;
	/*!
	 * Adds a new Entry to the domain. Here the shared_ptr is made, and the Entry is pushed to the store.
	 * @param user username
	 * @param pass password for entry
	 * @return this
	 */
	Domain & Add( const string & user, const string & pass );
	/*!
	 * Adding only a "new" shared pointer to the vector of Enties. Done when there is another Entry with given data.
	 * @param entry pointer to the given Entry
	 * @return this for more
	 */
	Domain & Add( shared_ptr<Entry> entry );
	/*!
	 * Inserts a note to the map of notes. The map is keyed by a username.
	 * when there already is a note in connection to the username, throws.
	 * @param user username as a key
	 * @param note string to be inserted
	 * @return this
	 */
	Domain & AddNote(const string & user, const string & note);
	/*!
	 * Changes an content of a note, in case there is already a note. If not, throws.
	 * @param user username as a key to change note
	 * @param note string to add to the map
	 * @return this
	 */
	Domain & OverwriteNote(const string & user, const string & note);
	/*!
	 * Checks, if there is a username to erase, if not, throws.
	 * Also erasing a note, if uname exists.
	 * @param user username as a link to erase
	 */
	void Erase (const string & user);
	/*!
	 * Erases a note for given username. If there is not the username, throws.
	 * @param user key to the note.
	 */
	void EraseNote(const string & user);
	/*!
	 * This funny method makes an Entry with given parameters, to be compared with the other Entries in the domain.
	 * If there is an Entry with given parameter, returns the shared_ptr.
	 * If there is not, returns nullptr.
	 * @param user username
	 * @param pass password
	 * @return smart pointer
	 */
	shared_ptr<Entry> GetEntry ( const string & user, const string & pass ) const;
	/*!
	 * This funny method returns a pointer to Entry with given username (only).
	 * If username is there not, returns nullptr.
	 * @param user username to find
	 * @return smart pointer.
	 */
	shared_ptr<Entry> GetEntry (const string & user ) const;
	/*!
	 * Prints roughly the whole domain. Is used only when developing.
	 * And for debugging
	 */
	void printRaw() const;
	/*!
	 * Prints the Whole Domain to an output stream. In tabbed way.
	 */
	void printWholeDomain ( ostream & os ) const;
	/*!
	 * finds a specific substring in a domain, also finding in the map of notes.
	 */
	void find(const string & toFind, ostream & os) const;
	/*!
	 * returns a vector of a nice, sorted Records, which are pretty much the same animals,
	 * but with rawer interpretation. Used when writing to file. This is how I keep my Entries sorted.
	 * @return vector of shared pointers.
	 */
	vector<shared_ptr<Record>> GetSortedRecords();

protected:
	vector<shared_ptr<Entry> > m_entries;
	unordered_map<string, string> m_notes;
	string m_name;
};


#endif //V3_DOMAIN_H
