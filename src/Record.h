//
// Created by antigravity on 5/26/17.
//

#ifndef V3_RECORD_H
#define V3_RECORD_H

#include "Entry.h"

/*!
 * Class for storing rawer Entry.
 */
class Record : public Entry
{
public:
	/*!
	 * inheriting from Entry, added other string - note - to insert to file
	 * @param user username
	 * @param pass password
	 * @param note simple string
	 */
	Record(const string & user, const string & pass, const string & note);
	/*!
	 * returns simple string, this is a lazy extension of the parent class.
	 * @return string name
	 */
	virtual string getNote() const;

protected:
	string m_note;
};


#endif //V3_RECORD_H
