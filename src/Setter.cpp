//
// Created by antigravity on 5/29/17.
//

#include "Setter.h"
#include "Crypter.h"
#include <unistd.h>

Setter::Setter(const vector<string> &vec)
		: Command(vec) {}
string Setter::GetName() const
{
	return "set";
}

void Setter::run()
{
	db.readFile(m_file);
	try {transform(m_args.at(2).begin(), m_args.at(2).end(), m_args.at(2).begin(), ::tolower);}catch(...){}

	if ( m_args.size() == 3 && m_args.at(1) == "-d" ) {
		m_domain = m_args.at(2);
	} else if ( m_args.size() == 4 && m_args.at(1) == "-u" ) {
		db.Change(m_domain,m_args.at(2),m_args.at(3));
		db.writeFile(m_file);
	} else if ( m_args.size() == 4 && m_args.at(1) == "-n" ) {
		db.OverwriteNote(m_domain,m_args.at(2),m_args.at(3));
		db.writeFile(m_file);
// 	} else if (m_args.size() == 2 && m_args.at(1) == "--encrypt") {
// //		char *pass=getpass("Enter password: ");
// 		Crypter cr;
// 		cr.Encrypt(m_file, "01234567890123456");
// 		cout << "encrypted"<<endl;
// 	} else if (m_args.size() == 2 && m_args.at(1) == "--decrypt") {
// //		char *pass=getpass("Enter password: ");
// 		Crypter dcr;
// 		dcr.Decrypt(m_file, "01234567890123456");
// 		cout << "decrypted"<<endl;
	} else {
		throw BadCommand("Invalid! Use with -d <> ; -p <><> ; -n <><>");
	}
}