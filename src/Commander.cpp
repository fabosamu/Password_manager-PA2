//
// Created by antigravity on 5/29/17.
//

#include "Commander.h"

Commander::Commander()
{}
Commander::~Commander()
{}

void Commander::makeCommand(unique_ptr<Command> && cmd)
{
	m_commands[cmd->GetName()] = move(cmd);
}

void Commander::doCommand(const string & com)
{
	checkCommand(com);
	m_commands.at(com).get()->getConfig();
	m_commands.at(com).get()->run();
	m_commands.at(com).get()->setConfig();
}

void Commander::checkCommand(const string &com)
{
	if ( m_commands.find(com) == m_commands.end() ){
		throw BadCommand();
	}
}