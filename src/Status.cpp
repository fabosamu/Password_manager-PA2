//
// Created by antigravity on 5/29/17.
//

#include "Status.h"

Status::Status(const vector<string> &vec)
		: Command(vec) {}

string Status::GetName() const
{
	return "status";
}

void Status::run()
{
	if ( m_args.size() == 1 ) {
		if (!m_file.empty()){
			cout << "->" << m_file << endl;
		}
		if (!m_domain.empty()){
			cout << "->" << m_domain << endl;
		}
	} else {
		throw BadCommand();
	}
}