# Password manager

This was a task from school, i was (t)asked to write 1000-3000 lines of working code, have a good makefile, documentation and nice design.

This CLI password manager is fully functional, can be used right away. 
There are several issues such as safety (not very clever encrypting) and command line interface as itself (not very extendable).

Under GPL: 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
